// ==UserScript==
// @name         Faceit-Analyzer
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       Marius Riedl
// @match        https://www.faceit.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function () {
  "use strict";

  let done = false;

  setInterval(function () {
    const isInMatchRoom = window.location.pathname.includes("room");

    if (!done && isInMatchRoom) {
      const analyserUrl = "https://faceitanalyser.com/lobbyanalyser/";
      const path = window.location.pathname;

      // gets game id from url, ids start with a 1
      const gameId = path.substring(path.indexOf("1"), path.indexOf("1") + 38);

      // gets match room navigation
      let nav = document.getElementsByClassName("fi-navbar__main__items")[1];

      function addAnalyserButton() {
        const href = analyserUrl + gameId;
        const tag = "Analyser";
        console.log("Creating link to: ", href);
        nav = nav.insertAdjacentHTML("beforeend", getFaceitButton(href, tag));
      }

      function getFaceitButton(href, tag) {
        // trust me it works
        const faceitButton =
          '<div class="fi-navbar__main__wrapper" ng-class="{ \'-spaced\': vm.overflow === \'wrap\' }"><fi-navbar-item"><a ng-class="\'fi-navbar__main__item\'" href="' +
          href +
          '" target="_blank" class="fi-navbar__main__item"><span>' +
          tag +
          "</span></a></fi-navbar-item></div>";

        return faceitButton;
      }

      addAnalyserButton();

      done = true;
    } else if (!isInMatchRoom) {
      // this resets the done state whenever someone leaves the match room
      done = false;
    }
  }, 500);
})();
